﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace TestProject
{
    [TestFixture]
    public class NUnitTes
    {
        private IWebDriver driver;

        [SetUp]
        public void SetupTest()
        {
            driver = new ChromeDriver(@"E:\Home\testing\chromedriver_win32");
        }

        [Test]
        public void GoogleTest()
        {         
            driver.Navigate().GoToUrl("http://www.google.com.ua/");
            Assert.AreEqual("Google", driver.Title);
          }

        [TearDown]
        public void TeardownTest()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
        }
        
    }
}