﻿Feature: CheckSearch
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers

@mytag
Scenario Outline: Check Search is working
	Given Homepage is opened
	When I type <search_word> into the search field and press Enter
	Then the page with search results should opened

	Examples: 
	| search_word |
	| nike        |
