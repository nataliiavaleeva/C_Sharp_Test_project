﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;

namespace TestProject
{
    [Binding]
    public class LoginSteps
    {
        public IWebDriver driver;

        [Given(@"Open the login page")]
        public void GivenOpenTheLoginPage()
        {
            driver = new ChromeDriver(@"E:\Home\testing\chromedriver_win32");
            driver.Url = "https://answear.ua/";
            driver.FindElement(By.Id("panelLogin")).Click();
        }
        
        [Given(@"I have entered (.*) into the Email field")]
        public void GivenIHaveEnteredIntoTheEmailField(string email)
        {
            driver.FindElement(By.Id("l1")).SendKeys(email);
            
        }
        
        [Given(@"I have entered (.*) into the Password field")]
        public void GivenIHaveEnteredIntoThePasswordField(string password)
        {
            driver.FindElement(By.Id("l2")).SendKeys(password);
        }
        
        [When(@"I press Submit button")]
        public void WhenIPressSubmitButton()
        {
            driver.FindElement(By.Id("loginFormSubmit")).Click();
        }
        
        [Then(@"the link named Аккаунт should be at the page")]
        public void ThenTheLinkNamedShouldBeAtThePage()
        {
            true.Equals(driver.FindElement(By.Id("panelAccount")).Displayed);
        }
    }
}
